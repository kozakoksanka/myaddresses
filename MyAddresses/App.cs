﻿using MvvmCross;
using MvvmCross.ViewModels;
using MyAddresses.Data.Persistence;
using MyAddresses.Data.Persistence.Repositories;
using MyAddresses.Data.Services;
using MyAddresses.Data.Services.Interfaces;
using MyAddresses.ViewModels;

namespace MyAddresses
{
    public class App : MvxApplication
    {
        public override async void Initialize()
        {
            Mvx.IoCProvider.RegisterSingleton(typeof(IDatabaseContext), new DatabaseContext());
            Mvx.IoCProvider.RegisterSingleton(typeof(IDatabaseService), new DatabaseService(Mvx.IoCProvider.Resolve<IDatabaseContext>()));
            Mvx.IoCProvider.RegisterSingleton(typeof(IAddressRepository), new AddressRepository(Mvx.IoCProvider.Resolve<IDatabaseContext>()));
            Mvx.IoCProvider.RegisterSingleton(typeof(IAddressService), new AddressService(Mvx.IoCProvider.Resolve<IAddressRepository>()));

            RegisterAppStart<FavoritesViewModel>();

            var databaseService = Mvx.IoCProvider.Resolve<IDatabaseService>();
            await databaseService.CreateDatabaseAsync();

            base.Initialize();
        }
    }
}
