﻿using System.Threading.Tasks;
using MyAddresses.Data.Persistence;
using MyAddresses.Data.Services.Interfaces;

namespace MyAddresses.Data.Services
{
    public class DatabaseService: IDatabaseService
    {
        private readonly IDatabaseContext _context;

        public DatabaseService(IDatabaseContext context)
        {
            _context = context;
        }
        public async Task<bool> CreateDatabaseAsync()
        {
            return await _context.CreateDatabaseAsync();
        }
    }
}
