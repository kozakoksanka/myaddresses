﻿using System.Threading.Tasks;

namespace MyAddresses.Data.Services.Interfaces
{
    public interface IDatabaseService
    {
        Task<bool> CreateDatabaseAsync();
    }
}
