﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MyAddresses.Data.Entities;

namespace MyAddresses.Data.Services.Interfaces
{
    public interface IAddressService
    {
        Task<List<Location>> GetAllAddressesAsync();
        Task<Location> GetAddressAsync(int id);
        Task <int> AddAddressAsync(Location address);
        Task <bool> UpdateAddressAsync(Location address);
        Task<int> DeleteAddressAsync(Location address);
    }
}
