﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MyAddresses.Data.Entities;
using MyAddresses.Data.Persistence.Repositories;
using MyAddresses.Data.Services.Interfaces;

namespace MyAddresses.Data.Services
{
    public class AddressService: IAddressService
    {
        private readonly IAddressRepository _addressRepository;

        public AddressService(IAddressRepository addressRepository)
        {
            _addressRepository = addressRepository;
        }

        public async Task<List<Location>> GetAllAddressesAsync()
        {
            return await _addressRepository.SelectAllAsync("Address");
        }

        public async Task<Location> GetAddressAsync(int id)
        {
            return await _addressRepository.FindByIdAsync(id, nameof(Location));
        }

        public async Task<int> AddAddressAsync(Location address)
        {
            return await _addressRepository.AddAsync(address);
        }

        public async Task<bool> UpdateAddressAsync(Location address)
        {
            return await _addressRepository.UpdateAddressAsync(address);
        }

        public async Task<int> DeleteAddressAsync(Location address)
        {
            return await _addressRepository.RemoveAsync(address);
        }
    }
}
