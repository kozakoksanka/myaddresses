﻿using SQLite;

namespace MyAddresses.Data.Entities
{
    [Table("Address")]
    public class Location: BaseEntity
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public double Lat { get; set; }
        public double Long { get; set; }
    }
}
