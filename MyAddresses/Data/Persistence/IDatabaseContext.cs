﻿using System.Threading.Tasks;
using SQLite;

namespace MyAddresses.Data.Persistence
{
    public interface IDatabaseContext
    {
        Task<bool> CreateDatabaseAsync();
        SQLiteAsyncConnection GetConnection();
    }
}
