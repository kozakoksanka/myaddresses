﻿using SQLite;
using System.IO;
using System.Threading.Tasks;
using MyAddresses.Data.Entities;


namespace MyAddresses.Data.Persistence
{
    public class DatabaseContext: IDatabaseContext
    {
        public DatabaseContext()
        {
        }

        private readonly string _dbLocation =
            System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);

        private readonly string _dbName = "MyAddresses.db";

        private SQLiteAsyncConnection _connection;

        public SQLiteAsyncConnection DbConnection =>
            _connection ?? (_connection = new SQLiteAsyncConnection(Path.Combine(_dbLocation, _dbName)));

        public async Task<bool> CreateDatabaseAsync()
        {
            try
            {
                await CreateTablesAsync();
                return true;
            }
            catch (SQLiteException ex)
            {
                return false;
            }
        }

        private async Task CreateTablesAsync()
        {
            await DbConnection.CreateTableAsync<Location>();
        }


        public SQLiteAsyncConnection GetConnection()
        {
            return DbConnection;
        }
    }
}