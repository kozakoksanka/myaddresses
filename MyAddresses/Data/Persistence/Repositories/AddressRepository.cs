﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MyAddresses.Data.Entities;
using MyAddresses.Data.Persistence.Repositories.Base;
using SQLite;

namespace MyAddresses.Data.Persistence.Repositories
{
    public class AddressRepository: Repository<Location>, IAddressRepository
    {
        private readonly IDatabaseContext _context;

        public AddressRepository(IDatabaseContext context) : base(context)
        {
            _context = context; 
        }

        public async Task<bool> UpdateAddressAsync(Location address)
        {
            try
            {
               await _context.GetConnection().QueryAsync<Location>(
                        "UPDATE Address set Name=?, Address=?, Lat=?, Long=? Where Id=?",
                        address.Name,
                        address.Address,
                        address.Lat,
                        address.Long,
                        address.Id);
               return true;
            }
            catch (SQLiteException ex)
            {
                return false;
            }
        }

        public async Task<int> AddAsync(Location entity)
        {
            return await base.AddAsync(entity);
        }

        public async Task<int> RemoveAsync(Location entity)
        {
            return await base.RemoveAsync(entity);
        }

        public async Task<Location> FindByIdAsync(int id, string tableName)
        {
            return await base.FindByIdAsync(id, tableName);
        }

        public async Task<List<Location>> SelectAllAsync()
        {
            return await base.SelectAllAsync("Address");
        }
    }
}