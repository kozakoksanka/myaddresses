﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MyAddresses.Data.Entities;

namespace MyAddresses.Data.Persistence.Repositories.Base
{
    public interface IRepository<TEntity> where TEntity : BaseEntity
    {
        Task<int> AddAsync(TEntity entity);
        Task<int> RemoveAsync(TEntity entity);
        Task<TEntity> FindByIdAsync(int id, string tableName);
        Task<List<TEntity>> SelectAllAsync(string tableName);
    }
}