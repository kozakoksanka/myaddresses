﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MyAddresses.Data.Entities;
using SQLite;

namespace MyAddresses.Data.Persistence.Repositories.Base
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity, new()
    {
        private readonly IDatabaseContext _context;

        public Repository(IDatabaseContext context)
        {
            _context = context;
        }

        public async Task<int> AddAsync(TEntity entity)
        {
            try
            {
                return await _context.GetConnection().InsertAsync(entity);
            }
            catch (SQLiteException ex)
            {
                return -1;
            }
        }

        public async Task<int> RemoveAsync(TEntity entity)
        {
            try
            {
                return await _context.GetConnection().DeleteAsync(entity);
            }
            catch (SQLiteException ex)
            {
                return -1;
            }
        }

        public async Task<TEntity> FindByIdAsync(int id, string tableName)
        {
            try
            {
                var result = await _context.GetConnection()
                    .QueryAsync<TEntity>("SELECT * FROM ? Where Id=?", tableName, id);
                return result.FirstOrDefault();
            }
            catch (SQLiteException ex)
            {
                return null;
            }
        }

        public async Task<List<TEntity>> SelectAllAsync(string tableName)
        {
            try
            {
                return await _context.GetConnection().QueryAsync<TEntity>("SELECT * FROM Address", tableName);
            }
            catch (SQLiteException ex)
            {
                return null;
            }
        }
    }
}