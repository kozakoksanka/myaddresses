﻿using System.Threading.Tasks;
using MyAddresses.Data.Entities;
using MyAddresses.Data.Persistence.Repositories.Base;

namespace MyAddresses.Data.Persistence.Repositories
{
    public interface IAddressRepository: IRepository<Location>
    {
        Task<bool> UpdateAddressAsync(Location address);
    }
}