﻿using System.Threading.Tasks;
using MvvmCross.Commands;
using MyAddresses.Data.Entities;

namespace MyAddresses.ViewModels
{
    public class SearchViewModel : BaseViewModel<object, Location>
    {
        private IMvxCommand<Location> _locationFoundCommand;
        public IMvxCommand<Location> LocationFoundCommand
        {
            get
            {
                return _locationFoundCommand ??
                       (_locationFoundCommand = new MvxCommand<Location>((location) => HandleLocation(location)));
            }
        }

        private void HandleLocation(Location location) 
        {
            Task.Delay(1).ContinueWith((arg) => Navigation.Close(this, location));
        }

        public override void Prepare(object parameter)
        {
            // Nothing to do
        }
    }
}
