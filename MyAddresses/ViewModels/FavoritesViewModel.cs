﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using MvvmCross.Commands;
using MyAddresses.Data.Entities;
using MyAddresses.Data.Services.Interfaces;

namespace MyAddresses.ViewModels
{
    public class FavoritesViewModel : BaseViewModel
    {
    
        private readonly IAddressService _addressService;

        public FavoritesViewModel (IAddressService addressService)
        {
            _addressService = addressService;
        }

        public override async Task Initialize()
        {
            var savedLocations = await _addressService.GetAllAddressesAsync();

            var collection = new ObservableCollection<Location>(savedLocations ?? new List<Location>());
            Locations = collection;

            Locations = new ObservableCollection<Location>(await _addressService.GetAllAddressesAsync());

            await base.Initialize();
        }

        private ObservableCollection<Location> _locations;
        public ObservableCollection<Location> Locations
        {
            get => _locations;
            set => SetProperty(ref _locations, value);
        }

        private Location _selectedLocation;
        public Location SelectedLocation
        {
            get => _selectedLocation;
            set => SetProperty(ref _selectedLocation, value);
        }

        private IMvxAsyncCommand _addFavoriteLocation;
        public IMvxAsyncCommand ShowPinDetailsCommand => _addFavoriteLocation ?? (_addFavoriteLocation = new MvxAsyncCommand(() => NavigateToSearch()));

        private IMvxCommand<Location> _locationSelectedCommand;
        public IMvxCommand<Location> LocationSelectedCommand => _locationSelectedCommand ?? (_locationSelectedCommand = new MvxCommand<Location>((location) => SelectedLocation = location));

        private async Task NavigateToSearch()
        {
            var result = await Navigation.Navigate<SearchViewModel, Location>();
            if (result != null)
            {
                Locations.Add(result);
                await _addressService.AddAddressAsync(result);
                SelectedLocation = result;

                RaisePropertyChanged(nameof(Locations));
            }
        }
    }
}