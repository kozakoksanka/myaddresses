﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using MvvmCross;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;

namespace MyAddresses.ViewModels
{
    public abstract class BaseViewModel : BaseViewModel<object, object>
    {
        public override void Prepare(object parameter = null)
        {
            base.Prepare();
        }
    }

    public abstract class BaseViewModel<T, TResult> : MvxViewModel<T, TResult>
    {
        protected readonly IMvxNavigationService Navigation;

        bool isBusy = false;
		public bool IsBusy
		{
			get => isBusy;
            set => SetProperty(ref isBusy, value);
        }

		string title = string.Empty;
		public string Title
		{
			get => title;
            set => SetProperty(ref title, value);
        }

		public BaseViewModel()
		{
			Navigation = Mvx.IoCProvider.Resolve<IMvxNavigationService>();
		}

		protected bool SetProperty<T>(ref T backingStore, T value,
			[CallerMemberName]string propertyName = "",
			Action onChanged = null)
		{
			if (EqualityComparer<T>.Default.Equals(backingStore, value))
				return false;

			backingStore = value;
			onChanged?.Invoke();
			RaisePropertyChanged(propertyName);
			return true;
		}
	}
}
