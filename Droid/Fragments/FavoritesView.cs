﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using Android.OS;
using Android.Runtime;
using Android.Views;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Android.Presenters.Attributes;
using MyAddresses.Data.Entities;
using MyAddresses.ViewModels;

namespace MyAddresses.Droid.Fragments
{
    [MvxFragmentPresentation(typeof(MainViewModel), Resource.Id.fragment_container)]
    [Register("myaddresses.droid.fragments.FavoritesView")]
    public class FavoritesView : BaseFragment<FavoritesViewModel>, IOnMapReadyCallback
    {
        private GoogleMap map;
        private bool IsMapLoaded;
        protected override int FragmentId => Resource.Layout.fragment_favorites;

        public ObservableCollection<Location> _locations;
        public ObservableCollection<Location> Locations
        {
            get => _locations;
            set => _locations = value;
        }

        private Location _selectedLocation;
        public Location SelectedLocation
        {
            get => _selectedLocation;
            set
            {
                _selectedLocation = value;

               DrawMarker(_selectedLocation);
            }
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = base.OnCreateView(inflater, container, savedInstanceState);
            SupportMapFragment fragment = (SupportMapFragment)ChildFragmentManager.FindFragmentById(Resource.Id.map);
            fragment.GetMapAsync(this);
            return view;
        }

        public override void OnStart()
        {
            base.OnStart();

            var set = this.CreateBindingSet<FavoritesView, FavoritesViewModel>();

            set.Bind().For(v => v.Locations).To(vm => vm.Locations);
            set.Bind().For(v => v.SelectedLocation).To(vm => vm.SelectedLocation);
            set.Apply();
        }

        public void OnMapReady(GoogleMap googleMap)
        {
            map = googleMap;
            Task.Delay(3000).ContinueWith((arg) => 
            { 
                IsMapLoaded = true; 
                DrawMarker(SelectedLocation);
            });
        }

        private void DrawMarker(Location location)
        {
            map.Clear();

            if (location == null || map == null || !IsMapLoaded)
                return;

            var markerOptions = new MarkerOptions().
            SetPosition(new LatLng(location.Lat, location.Long)).
            SetTitle(location.Name);

            map.AddMarker(markerOptions);
            ShowPoints(new LatLng(location.Lat, location.Long));
        }

        private void ShowPoints(LatLng point)
        {
            var builder = new LatLngBounds.Builder();
                builder.Include(point);


            map.AnimateCamera(CameraUpdateFactory.NewLatLngBounds(builder.Build(), 100));
        }
    }
}
