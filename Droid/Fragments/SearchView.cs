﻿using Android.OS;
using Android.Views;
using MvvmCross.Platforms.Android.Presenters.Attributes;
using MyAddresses.ViewModels;
using Google.Places;
using System.Collections.Generic;
using Android.Runtime;
using MyAddresses.Data.Entities;

namespace MyAddresses.Droid.Fragments
{
    [MvxFragmentPresentation(typeof(MainViewModel), Resource.Id.fragment_container, true)]
    [Register("myaddresses.droid.fragments.SearchView")]
    public class SearchView : BaseFragment<SearchViewModel>
    {
        private AutocompleteSupportFragment autocompleteFragment;
      
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            if (!PlacesApi.IsInitialized) 
            {
                PlacesApi.Initialize(this.Context, GetString(Resource.String.google_maps_key));
            }
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = base.OnCreateView(inflater, container, savedInstanceState); 

            autocompleteFragment = (AutocompleteSupportFragment) ChildFragmentManager.FindFragmentById(Resource.Id.autocomplete_fragment);

            // Specify the types of place data to return.
            autocompleteFragment.SetPlaceFields(new List<Place.Field>
            {
                Place.Field.Id,
                Place.Field.Name,
                Place.Field.LatLng,
                Place.Field.Address
            });

            // Set up a PlaceSelectionListener to handle the response.
            autocompleteFragment.PlaceSelected += (sender, e) => 
            {
                var place = e.P0;
                var item = new Location
                {
                    Name = place.Name,
                    Address = place.Address,
                    Lat = place.LatLng.Latitude,
                    Long = place.LatLng.Longitude
                };
                Activity.RunOnUiThread(() =>
                {
                    if (ViewModel.LocationFoundCommand.CanExecute(item))
                    ViewModel.LocationFoundCommand.Execute(item);
                });
            };

            return view;
        }

        protected override int FragmentId => Resource.Layout.search_fragment;
    }
}