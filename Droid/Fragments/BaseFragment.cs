﻿using Android.OS;
using Android.Views;
using MvvmCross.Droid.Support.V4;
using MvvmCross.Platforms.Android.Binding.BindingContext;
using MvvmCross.ViewModels;

namespace MyAddresses.Droid.Fragments
{
    public abstract class BaseFragment : MvxFragment
    {
        public MvxFragmentActivity ParentActivity => (MvxFragmentActivity)Activity;

        protected BaseFragment()
        {
            RetainInstance = true;
        }

		public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var ignore = base.OnCreateView(inflater, container, savedInstanceState);

            var view = this.BindingInflate(FragmentId, null);

			return view;
        }

        protected abstract int FragmentId { get; }
	}

    public abstract class BaseFragment<TViewModel> : BaseFragment where TViewModel : class, IMvxViewModel
    {
        public new TViewModel ViewModel
        {
            get => (TViewModel)base.ViewModel;
            set => base.ViewModel = value;
        }
    }
}
