﻿using System;
using System.Collections;
using System.Globalization;
using MvvmCross.Converters;

namespace MyAddresses.Droid.Converters
{
    public class CollectionToBoolConverter : MvxValueConverter<IList, bool>
    {
        protected override bool Convert(
            IList value, 
            Type targetType, 
            object parameter, 
            CultureInfo culture) => value?.Count != 0;
    }
}