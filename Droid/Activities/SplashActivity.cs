﻿using Android.App;
using MvvmCross.Droid.Support.V7.AppCompat;
using MvvmCross.Platforms.Android.Presenters.Attributes;

namespace MyAddresses.Droid.Activities
{
    [MvxActivityPresentation]
    [Activity(Label = "@string/app_name", Theme = "@style/SplashTheme", MainLauncher = true)]
    public class SplashActivity : MvxSplashScreenAppCompatActivity
    {
        // nothing to do.
    }
}
