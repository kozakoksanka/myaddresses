﻿using Android.OS;
using Android.Support.V7.App;
using Android.Support.V7.Widget;

namespace MyAddresses.Droid.Activities
{
    public class BaseActivity : AppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(LayoutResource);
        }

        protected virtual int LayoutResource
        {
            get;
        }
    }
}
