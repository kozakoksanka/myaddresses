﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using MvvmCross.Droid.Support.V7.AppCompat;

using MyAddresses.Droid.Converters;

namespace MyAddresses.Droid
{
    public class Setup : MvxAppCompatSetup<App>
    {
        public Setup()
        {
        }

        protected override IEnumerable<Assembly> ValueConverterAssemblies
        {
            get
            {
                var toReturn = base.ValueConverterAssemblies.ToList();

                toReturn.Add(typeof(CollectionToBoolConverter).Assembly);

                return toReturn;
            }
        }
    }
}